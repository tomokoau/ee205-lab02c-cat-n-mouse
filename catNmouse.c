///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Tomoko Austin <tomokoau@hawaii.edu>
/// @date    26_Jan_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#define DEFAULT_MAX_NUMBER (2048)
#define DEBUG

int main( int argc, char* argv[] ) {
   //prompt to user what the range is 
   int theMaxValue=DEFAULT_MAX_NUMBER,aGuess;
   // if argc is equal to 2 then user passed a value
   if (argc==2)
   //atoi = acii to int   
   { theMaxValue= atoi(argv[1]);}
   int theNumberImThinkingOf = rand()%theMaxValue +1;
   printf("this is the random number %d\n",theNumberImThinkingOf);
   // This is an example of getting a number from a user
   // Note:  Don't enter unexpected values like 'blob' or 1.5.
   do{
   printf( "OK cat, I'm thinking of a number from 1 to %d\n",theMaxValue); 
   printf( "Make a guess: " );
   scanf( "%d", &aGuess );

   if (aGuess>=1 && aGuess <=theMaxValue){
      if(aGuess < theNumberImThinkingOf){ printf("No cat... the number I'm thinking of is larger than %d\n",aGuess);
      }
      if(aGuess >theNumberImThinkingOf){ printf("No cat ... the number I'm thinking of is smaller than %d\n",aGuess);
      }
      if(aGuess == theNumberImThinkingOf){ printf("You got me.\n /\\_/\\\n( o.o )\n > ^ <\n");
      }
   }
   else{ printf("error: please enter a number from 1 to %d\n",theMaxValue);
   }
   }
   while(aGuess != theNumberImThinkingOf);
   printf( "The number was [%d]\n", aGuess );
   
   return 1;  // This is an example of how to return a 1
}

